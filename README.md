# terraform-kubernetes-provider-exec-plugin-eks
The [documentation](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs#exec-plugins) of the
kubernetes provider for terraform recommends to use exec plugin instead of token for EKS due to short-lived
authentication tokens that can expire relatively quickly.

The example in the documentation uses a full-blown aws cli to get new tokens each time the provider is used. The AWS CLI
v2 is about 130MB at the moment and requires a proper glibc (so, it does not work in alpine images), less and groff.
This is not very handy for CI environments where we would like to keep the images as small as possible.

This project is written in go and using the token package of the official [aws-iam-authenticator project](https://github.com/kubernetes-sigs/aws-iam-authenticator)
to fetch tokens of the EKS cluster for the kubernetes provider. It gets built into a single binary (~13MB) which can be
used in the exec plugin.

# usage
## get the binary
### download it from the release
```bash
export PLUGIN_VERSION=0.1.3
curl -L -o terraform-kubernetes-provider-exec-plugin-eks https://gitlab.com/api/v4/projects/26140791/packages/generic/terraform-kubernetes-provider-exec-plugin-eks/${PLUGIN_VERSION}/terraform-kubernetes-provider-exec-plugin-eks-linux-amd64-${PLUGIN_VERSION}
chmod 755 terraform-kubernetes-provider-exec-plugin-eks
```

### build the binary
```bash
git clone git@gitlab.com:bzachar/terraform-kubernetes-provider-exec-plugin-eks.git
cd terraform-kubernetes-provider-exec-plugin-eks
CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' .
```
## Example terraform config when the binary is in the same directory 
```yaml
provider "aws" {
  region = "eu-central-1"
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.example.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.example.certificate_authority.0.data)
  exec {
    api_version = "client.authentication.k8s.io/v1alpha1"
    args        = ["-clustername", local.eks_cluster_name]
    command     = "./terraform-kubernetes-provider-exec-plugin-eks"
  }
}

locals {
  eks_cluster_name = "eks-test"
}

data "aws_eks_cluster" "example" {
  name = local.eks_cluster_name
}

data "kubernetes_all_namespaces" "allns" {}

data "kubernetes_config_map" "example" {
  metadata {
    name      = "aws-auth"
    namespace = "kube-system"
  }
}

output "all-ns" {
  value = data.kubernetes_all_namespaces.allns.namespaces
}

output "configmap" {
  value = data.kubernetes_config_map.example
}

```


# Alternate solutions that I learned till now
I did not verify if they work as we are using our binary 
## [kubegrunt eks token](https://github.com/gruntwork-io/kubergrunt/): 44.7 MB
## [aws-iam-authenticator](https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html): 38.8 MB
