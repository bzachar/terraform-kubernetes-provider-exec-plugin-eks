module gitlab.com/bzachar/terraform-kubernetes-provider-exec-plugin-eks

go 1.15

require (
	github.com/aws/aws-sdk-go v1.38.25
	sigs.k8s.io/aws-iam-authenticator v0.5.2
)
